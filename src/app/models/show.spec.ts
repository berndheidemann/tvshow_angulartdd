import { Show } from './show';

describe('Show', () => {
  it('should create an empty instance', () => {
    expect(new Show()).toBeTruthy();
  });

  it('should have a constructor with label property', () => {
    const show = new Show('foo');

    expect(show.label).toBe('foo');
  });

  it('should have a pictureUrl and description property', () => {
    const show = new Show('foo');
    show.pictureUrl = 'bar';
    show.description = 'foobar';
    expect(show.pictureUrl).toBe('bar');
    expect(show.description).toBe('foobar');
  });

  it('should have a rating property', () => {
    const show = new Show('foo', 9.2);
    expect(show.rating).toBe(9.2, 0.001);
  });

  it('should set rating to 0 if given as NaN', () => {
    const show = new Show('foo', NaN);
    expect(show.rating).toBe(0, 0.001);

  });
});
