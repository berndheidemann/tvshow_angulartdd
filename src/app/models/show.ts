export class Show {

    pictureUrl: string;
    description: string;

    constructor(public label?: string, public _rating?: number) {
        if (!_rating) {
            this._rating = 0;
        }
    }

    set rating(rating: number) {
        if (!rating) {
            this._rating = 0;
        } else {
            this._rating = rating;
        }
    }

    get rating(): number {
        return this._rating;
    }
}
