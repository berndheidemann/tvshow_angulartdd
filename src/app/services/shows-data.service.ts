import { Injectable } from '@angular/core';
import { Show } from '../models/show';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShowsDataService {

  private _detailShow: Show;

  public _shows: Show[] = [];

  constructor(public http: HttpClient) {
    this.shows.push(new Show('Game of Thrones', 9.4));
    this.shows.push(new Show('Breaking Bad', 9.3));
  }

  get shows(): Show[] {
    return this._shows;
  }

  set shows(shows: Show[]) {
    this._shows = shows;
  }

  async add(tvShow: Show) {
    if (await this.checkIfExists(tvShow)) {
      this.loadShowDetailFromAPI(tvShow);
      this._shows.push(tvShow);
      return true;
    }
    return false;
  }

  delete(show: Show) {
    this._shows = this._shows.filter(s => s.label !== show.label);
  }

  async showDetail(show: Show) {
    await this.loadShowDetailFromAPI(show);

    this._detailShow = show;
  }

  get detailShow(): Show {
    return this._detailShow;
  }

  private async checkIfExists(show: Show) {
    try {
      const data = await this.http.get(`http://api.tvmaze.com/singlesearch/shows?q=${show.label}`).toPromise();
      return data && data['summary'];
    } catch (e) {
      return false;
    }
  }

  private async loadShowDetailFromAPI(show: Show) {
    try {
      const data = await this.http.get(`http://api.tvmaze.com/singlesearch/shows?q=${show.label}`).toPromise();
      show.pictureUrl = data['image']['medium'];
      show.description = data['summary'];
      show.label = data['name'];
      show.rating = parseFloat(data['rating']['average']);
    } catch (e) {

    }
  }

}
