import { TestBed, inject, getTestBed } from '@angular/core/testing';

import { ShowsDataService } from './shows-data.service';
import { Show } from '../models/show';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { AppModule } from '../app.module';

const breakingBadSearchReturn = `
{
  "id":169,
  "url":"http://www.tvmaze.com/shows/169/breaking-bad",
  "name":"Breaking Bad",
  "type":"Scripted",
  "language":"English",
  "genres":["Drama","Crime","Thriller"],
  "status":"Ended",
  "runtime":60,
  "premiered":"2008-01-20",
  "officialSite":"http://www.amc.com/shows/breaking-bad",
  "schedule":{"time":"22:00","days":["Sunday"]},
  "rating":{"average":9.3},
  "weight":94,
  "network": {
      "id":20,
      "name":"AMC",
      "country":{
          "name":"United States",
          "code":"US",
          "timezone":
          "America/New_York"
        }
      },
    "webChannel":null,
    "externals":{
      "tvrage":18164,
      "thetvdb":81189,
      "imdb":"tt0903747"
    },
    "image":{
      "medium":"http://static.tvmaze.com/uploads/images/medium_portrait/0/2400.jpg",
      "original":"http://static.tvmaze.com/uploads/images/original_untouched/0/2400.jpg"
    },
    "summary":"<p><b>Breaking Bad</b> follows protagonist Walter White, a chemistry teacher who lives in New Mexico with his wife and teenage son who has cerebral palsy. White is diagnosed with Stage III cancer and given a prognosis of two years left to live. With a new sense of fearlessness based on his medical prognosis, and a desire to secure his family's financial security, White chooses to enter a dangerous world of drugs and crime and ascends to power in this world. The series explores how a fatal diagnosis such as White's releases a typical man from the daily concerns and constraints of normal society and follows his transformation from mild family man to a kingpin of the drug trade.</p>",
    "updated":1534515563,
    "_links":{
      "self":{
        "href":"http://api.tvmaze.com/shows/169"
      },
      "previousepisode":{
        "href":"http://api.tvmaze.com/episodes/12253"
      }
    }
  }`;


function httpGetReturn(value) {
  return () => {
    return {
      toPromise: () => {
        return value ? JSON.parse(value) : value;
      }
    };
  };
}

function notFoundhttpGetReturn() {
  return () => {
    return {
      toPromise: () => {
        throw new Error('HttpErrorResponse');
      }
    };
  };
}

describe('ShowsDataService', () => {
  let service: ShowsDataService;
  const sampleTvShows = [new Show('foo'), new Show('bar')];



  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    });
    service = TestBed.get(ShowsDataService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should hold a list of shows', () => {
    expect(service.shows).toBeDefined();
    expect(service.shows instanceof Array).toBeTruthy();
  });

  it('should add a show', async () => {
    spyOn(service.http, 'get').and.callFake(httpGetReturn(breakingBadSearchReturn));
    service.shows = JSON.parse(JSON.stringify(sampleTvShows));
    const erg = await service.add(new Show('Breaking Bad'));
    expect(service.http.get).toHaveBeenCalled();
    expect(service.shows.length).toBe(3);
    expect(service.shows.filter(t => t.label === 'Breaking Bad').length).toBe(1);
    expect(erg).toBeTruthy();
  });

  it('should deny to a add a nonExistent show', async () => {
    spyOn(service.http, 'get').and.callFake(notFoundhttpGetReturn());
    service.shows = JSON.parse(JSON.stringify(sampleTvShows));
    const erg = await service.add(new Show('foobarfoooooo'));
    expect(service.shows.length).toBe(2);
    expect(service.shows.filter(t => t.label === 'foobarfoooooo').length).toBe(0);
    expect(erg).toBeFalsy();
  });

  it('should delete a show', () => {
    service.shows = JSON.parse(JSON.stringify(sampleTvShows));
    service.delete(sampleTvShows[0]);
    expect(service.shows.length).toBe(1);
    expect(service.shows.filter(t => t.label === 'foo').length).toBe(0);
    expect(service.shows.filter(t => t.label === 'bar').length).toBe(1);
  });

  it('should set a detailShow', async () => {
    spyOn(service.http, 'get').and.callFake(httpGetReturn(breakingBadSearchReturn));
    await service.showDetail(new Show('Breaking'));
    expect(service.detailShow.label).toBeDefined();
    expect(service.detailShow.label).toBe('Breaking Bad');
  });

  it('should have a http client', () => {
    expect(service.http).toBeDefined();
  });

  it('should get ShowDetails from TVMAZE API', async () => {
    spyOn(service.http, 'get').and.callFake(httpGetReturn(breakingBadSearchReturn));
    const show = new Show('Breaking');
    await service.showDetail(show);
    expect(service.http.get).toHaveBeenCalled();
    expect(service.detailShow.pictureUrl).toBe('http://static.tvmaze.com/uploads/images/medium_portrait/0/2400.jpg');
    expect(service.detailShow.description).toBe(JSON.parse(breakingBadSearchReturn).summary);
    expect(service.detailShow.label).toBe('Breaking Bad');
    expect(service.detailShow.rating).toBe(9.3, 0.01);
  });

  it('should catch error while loading ShowDetails from TVMAZE API if show doesnt exist', async () => {
    spyOn(service.http, 'get').and.callFake((notFoundhttpGetReturn()));
    const show = new Show('foobar');
    await service.showDetail(show);
    expect(service.http.get).toHaveBeenCalled();
    expect(show.pictureUrl).toBeUndefined();
    expect(show.description).toBeUndefined();
  });

});
