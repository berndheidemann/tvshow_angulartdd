import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDetailComponent } from './show-detail.component';
import { ShowsDataService } from '../../services/shows-data.service';
import { AppModule } from '../../app.module';
import { Show } from '../../models/show';

describe('ShowDetailComponent', () => {
  let component: ShowDetailComponent;
  let fixture: ComponentFixture<ShowDetailComponent>;
  let showsDataService: ShowsDataService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    showsDataService = TestBed.get(ShowsDataService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });



  it('should have a showDetail-Prop, from showsDataService', () => {
    spyOnProperty(showsDataService, 'detailShow').and.returnValue(new Show('foo'));
    const foo = component.detailShow;
    expect(foo.label).toBe('foo');
  });

});
