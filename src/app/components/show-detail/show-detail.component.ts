import { Component, OnInit } from '@angular/core';
import { ShowsDataService } from '../../services/shows-data.service';
import { Show } from '../../models/show';

@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.css']
})
export class ShowDetailComponent implements OnInit {

  constructor(private showsDataService: ShowsDataService) { }

  ngOnInit() {
  }

  get detailShow(): Show {
    return this.showsDataService.detailShow;
  }

}
