import { Component, OnInit } from '@angular/core';
import { ShowsDataService } from '../../services/shows-data.service';
import { Show } from '../../models/show';

@Component({
  selector: 'app-add-show-form',
  templateUrl: './add-show-form.component.html',
  styleUrls: ['./add-show-form.component.css']
})
export class AddShowFormComponent implements OnInit {

  show: Show;
  errorMessage: string;

  constructor(private showsDataService: ShowsDataService) {
    this.show = new Show();
  }

  ngOnInit() {
  }

  async save() {
    if (await this.showsDataService.add(this.show)) {
      this.show = new Show();
    } else {
      this.showError('TV-Serie existiert nicht!');
    }
  }

  showError(error: string) {
    this.errorMessage = error;
    setTimeout(() => { this.errorMessage = ''; }, 1000);
  }


}
