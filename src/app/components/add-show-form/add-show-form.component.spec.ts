import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AddShowFormComponent } from './add-show-form.component';
import { ShowsDataService } from '../../services/shows-data.service';
import { Show } from '../../models/show';
import { AppModule } from '../../app.module';

describe('AddTvshowFormComponent', () => {
  let component: AddShowFormComponent;
  let fixture: ComponentFixture<AddShowFormComponent>;
  let showsDataService: ShowsDataService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddShowFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    showsDataService = TestBed.get(ShowsDataService);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a show Prop', () => {
    expect(component.show).toBeDefined();
  });

  it('should have a save-method and delegate to tvshowDataService', async () => {
    spyOn(showsDataService, 'add').and.callFake(() => true);
    component.show.label = 'foo';
    await component.save();
    expect(component.show.label).toBe(new Show().label);
    expect(showsDataService.add).toHaveBeenCalled();

  });

  it('should display error if show doesnt exists', async () => {
    spyOn(showsDataService, 'add').and.callFake(() => false);
    spyOn(component, 'showError').and.callThrough();
    component.show.label = 'foo';
    await component.save();
    expect(component.show.label).toBe('foo');
    expect(showsDataService.add).toHaveBeenCalled();
    expect(component.showError).toHaveBeenCalled();
    expect(component.errorMessage).toBe('TV-Serie existiert nicht!');
  });

  it('should hide the error after 1000ms', fakeAsync(() => {
    component.showError('fooBar');
    expect(component.errorMessage).toBe('fooBar');
    tick(1001);
    expect(component.errorMessage).toBe('');
  }));


});
