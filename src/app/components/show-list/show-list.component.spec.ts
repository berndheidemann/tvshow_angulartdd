import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowListComponent } from './show-list.component';
import { Show } from '../../models/show';
import { AppModule } from '../../app.module';
import { ShowsDataService } from '../../services/shows-data.service';

describe('TvshowListComponent', () => {
  let component: ShowListComponent;
  let fixture: ComponentFixture<ShowListComponent>;
  let showsDataService: ShowsDataService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    showsDataService = TestBed.get(ShowsDataService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a list of tvshows', () => {
    expect(component.shows).toBeDefined();
    expect(component.shows instanceof Array).toBeTruthy();
  });

  it('should get the tvshows from service', () => {
    spyOnProperty(showsDataService, 'shows').and.returnValue([new Show('foo')]);
    expect(component.shows.length).toBe(1);
    expect(component.shows[0].label).toBe('foo');
  });

  it('should delegate a delete to showDataService', () => {
    spyOn(showsDataService, 'delete');
    component.deleteShow(new Show('foo'));
    expect(showsDataService.delete).toHaveBeenCalled();
  });

  it('should delegate a showDetail to showDataService', () => {
    spyOn(showsDataService, 'showDetail');
    component.showDetail(new Show('foo'));
    expect(showsDataService.showDetail).toHaveBeenCalled();
  });



});
