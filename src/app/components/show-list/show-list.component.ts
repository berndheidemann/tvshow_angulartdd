import { Component, OnInit } from '@angular/core';
import { Show } from '../../models/show';
import { ShowsDataService } from '../../services/shows-data.service';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnInit {


  constructor(private showsDataService: ShowsDataService) {

  }

  ngOnInit() {
  }

  get shows(): Show[] {
    return this.showsDataService.shows;
  }

  deleteShow(show: Show) {
    this.showsDataService.delete(show);
  }

  showDetail(show: Show) {
    this.showsDataService.showDetail(show);
  }

}
