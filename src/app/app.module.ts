import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ShowListComponent } from './components/show-list/show-list.component';
import { AddShowFormComponent } from './components/add-show-form/add-show-form.component';
import { ShowsDataService } from './services/shows-data.service';
import { ShowDetailComponent } from './components/show-detail/show-detail.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ShowListComponent,
    AddShowFormComponent,
    ShowDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ShowsDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
